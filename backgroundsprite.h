#ifndef BACKGROUNDSPRITE_H
#define BACKGROUNDSPRITE_H

#include <SFML/Graphics.hpp>

#include "resources.h"


class BackgroundSprite : public sf::Drawable
{
public:
    BackgroundSprite();

protected:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

    sf::Sprite m_background;
    sf::Sprite m_logo;
    sf::Sprite m_board;
    sf::Sprite m_time;
};

#endif // BACKGROUNDSPRITE_H
