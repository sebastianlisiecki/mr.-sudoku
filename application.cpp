#include "application.h"

#include <windows.h>
#include <fstream>
#include <iostream>

#include <QDebug>

Application::Application() :
    m_window(sf::VideoMode(900, 600), "Mr. Sudoku", sf::Style::Close)
{

}

void Application::run()
{
    initialize();

    while (m_window.isOpen())
    {
        while (m_window.pollEvent(m_events))
        {
            handleEvents();
        }

        update();
        draw();
    }
}

void Application::initialize()
{
    m_timeText.setFont(Resources::getFont());
    m_timeText.setFillColor(sf::Color::Black);
    m_timeText.setPosition(150, 250);
    m_timeText.setCharacterSize(50);
    m_timeText.setString("0");

    m_statementText.setFont(Resources::getFont());
    m_statementText.setFillColor(sf::Color::Black);
    m_statementText.setCharacterSize(30);
    m_statementText.setString("Congratulations!");
    m_statementText.setPosition(30, 430);

    m_saveButton.setTexture(Resources::getTexture(TextureType::ButtonSave));
    m_loadButton.setTexture(Resources::getTexture(TextureType::ButtonLoad));

    m_saveButton.setPosition(37, 334);
    m_loadButton.setPosition(172, 334);

}

void Application::handleEvents()
{
    switch (m_events.type)
    {
    case sf::Event::Closed:
        m_window.close();
        break;

    case sf::Event::TextEntered:
    {
        if (m_end)
            return;

        char keyPressed = m_events.text.unicode;
        if (keyPressed < '1' || keyPressed > '9')
            return;

        if (m_board.currentCell().isDefault())
            return;

        int value = keyPressed - '0';

        if (!m_board.currentCell().isSelected())
            m_board.increaseFilledCellsCount();

        m_board.currentCell().setValue(value);
        break;
    }
    case sf::Event::KeyPressed:
    {
        if (m_events.key.code == sf::Keyboard::Escape)
            m_window.close();

        if (m_end)
            return;

        switch (m_events.key.code)
        {
        case sf::Keyboard::Delete:
            if (m_board.currentCell().isDefault())
                return;

            m_board.currentCell().clear();
            m_board.decreaseFilledCellsCount();
            break;

        case sf::Keyboard::BackSpace:
            if (m_board.currentCell().isDefault())
                return;

            m_board.currentCell().clear();
            m_board.decreaseFilledCellsCount();
            break;

        case sf::Keyboard::Up:
            if (m_board.getCurrentCellPosition().y <= 0)
                break;

            m_board.moveCurrentCell(Movement::Up);
            break;

        case sf::Keyboard::Right:
            if (m_board.getCurrentCellPosition().x >= m_board.numbersInRowCount() - 1)
                break;

            m_board.moveCurrentCell(Movement::Right);
            break;

        case sf::Keyboard::Down:
            if (m_board.getCurrentCellPosition().y >= m_board.numbersInRowCount() - 1)
                break;

            m_board.moveCurrentCell(Movement::Down);
            break;

        case sf::Keyboard::Left:
            if (m_board.getCurrentCellPosition().x <= 0)
                break;

            m_board.moveCurrentCell(Movement::Left);
            break;

        default:
            break;
        }

        break;
    }

    case sf::Event::MouseMoved:
    {
        HCURSOR Cursor;

        if (m_saveButton.getGlobalBounds().contains(m_events.mouseMove.x, m_events.mouseMove.y) ||
                m_loadButton.getGlobalBounds().contains(m_events.mouseMove.x, m_events.mouseMove.y))
            Cursor = LoadCursor(NULL, IDC_HAND);
        else
            Cursor = LoadCursor(NULL, IDC_ARROW);

        sf::WindowHandle wHandle;
        wHandle = m_window.getSystemHandle();
        SetCursor(Cursor);
        SetClassLongPtr(wHandle, GCLP_HCURSOR, reinterpret_cast<LONG_PTR>(Cursor));

        break;
    }

    case  sf::Event::MouseButtonPressed:
    {
        if (m_saveButton.getGlobalBounds().contains(m_events.mouseButton.x, m_events.mouseButton.y))
        {
            qDebug() << "Save";
            saveGame();
        }
        if (m_loadButton.getGlobalBounds().contains(m_events.mouseButton.x, m_events.mouseButton.y))
        {
            qDebug() << "Load";
            loadGame();
        }
    }

    default:
        break;
    }
}

void Application::update()
{
    if (m_end)
        return;

    m_timeElapsedFromLastUpdate += m_clock.restart().asSeconds();

    if (m_timeElapsedFromLastUpdate >= 1)
    {
        m_time++;

        sf::FloatRect rect(40, 232, 251, 75);
        m_timeText.setString(std::to_string(m_time));
        m_timeText.setPosition(rect.left + (rect.width / 2) - (m_timeText.getLocalBounds().width / 2),
                               rect.top + (rect.height / 2) - (m_timeText.getLocalBounds().height / 2));

        m_timeElapsedFromLastUpdate = 0;
    }

    const int maxCellsCount = 81;
    if (m_board.filledCellsCount() == maxCellsCount)
    {
        if (m_board.isResolved())
            m_end = true;
    }
}

void Application::draw()
{
    m_window.clear();

    m_window.draw(m_background);
    m_window.draw(m_timeText);
    m_window.draw(m_board);
    m_window.draw(m_saveButton);
    m_window.draw(m_loadButton);

    if (m_end)
    {
        m_window.draw(m_statementText);
    }

    m_window.display();
}

void Application::saveGame()
{
    std::fstream fileStream;

    fileStream.open("save.sud", std::ios::out);

    if (!fileStream.is_open())
    {
        qDebug() << "Can't open file.";
        return;
    }

    fileStream << m_time << std::endl;

    for (int i = 0; i < m_board.numbersInRowCount(); ++i)
    {
        for (int j = 0; j < m_board.numbersInRowCount(); ++j)
        {
            if (m_board.getCell(j, i).isDefault())
            {
                fileStream << "default" << std::endl;
                fileStream << m_board.getCell(j, i).value << std::endl;
            }
            else if (m_board.getCell(j, i).isSelected())
            {
                fileStream << "selected" << std::endl;
                fileStream << m_board.getCell(j, i).userValue() << std::endl;
            }
            else
            {
                fileStream << "empty" << std::endl;
                fileStream << 0 << std::endl;
            }
        }
    }

    fileStream.close();
}

void Application::loadGame()
{
    std::fstream fileStream;
    fileStream.open("save.sud", std::ios::in);

    if (!fileStream.is_open())
    {
        qDebug() << "Can't open file.";
        return;
    }

    fileStream.seekg(0, std::ios::end);
    int length = fileStream.tellg();

    if (length == 0)
    {
        qDebug() << "Invalid save file.";
        return;
    }

    fileStream.seekg(0);

    std::string line;

    std::getline(fileStream, line);
    qDebug() << line.c_str();

    m_time = std::stoi(line);
    m_timeText.setString(line);

    for (int i = 0; i < m_board.numbersInRowCount(); ++i)
    {
        for (int j = 0; j < m_board.numbersInRowCount(); ++j)
        {
            SudokuCell& currentCell = m_board.getCell(i, j);
            currentCell.reset();

            std::getline(fileStream, line);

            if (line == "default")
                currentCell.setDefault();
            else if (line == "selected")
                currentCell.setSelected();

            std::getline(fileStream, line);
            currentCell.setValue(std::stoi(line));
        }
    }

    fileStream.close();
    m_timeElapsedFromLastUpdate = 0.0f;
}
