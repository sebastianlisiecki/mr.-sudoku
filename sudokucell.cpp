#include "sudokucell.h"

#include <QDebug>

SudokuCell::SudokuCell(const sf::Vector2f &size) :
    m_size(size)
{
    m_textValue.setCharacterSize(40);
    m_textValue.setFillColor(sf::Color::Black);
}

void SudokuCell::setValue(const int &value)
{
    if (value == 0)
        m_textValue.setString("");
    else
        m_textValue.setString(std::to_string(value));

    m_textValue.setPosition(m_position.x + (m_size.x / 2) - (m_textValue.getLocalBounds().width / 2), m_position.y);
    m_selected = true;
}

void SudokuCell::setDefault()
{
    m_default = true;
    m_textValue.setFillColor(sf::Color(191, 35, 100));
}

void SudokuCell::setSelected()
{
    m_selected = true;
}

void SudokuCell::clear()
{
    m_selected = false;
    m_textValue.setString("");
}

void SudokuCell::reset()
{
    m_default = false;
    m_textValue.setFillColor(sf::Color::Black);
    clear();
}

void SudokuCell::setFont(const sf::Font &font)
{
    m_textValue.setFont(font);
}

void SudokuCell::setPosition(const sf::Vector2f &position)
{
    m_position = position;
}

void SudokuCell::setPosition(const float &x, const float &y)
{
    m_position.x = x;
    m_position.y = y;
}

void SudokuCell::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(m_textValue);
}

sf::Vector2f SudokuCell::position() const
{
    return m_position;
}

bool SudokuCell::isDefault() const
{
    return m_default;
}

bool SudokuCell::isSelected() const
{
    return m_selected;
}

int SudokuCell::userValue() const
{
    return std::stoi(m_textValue.getString().toAnsiString());
}
