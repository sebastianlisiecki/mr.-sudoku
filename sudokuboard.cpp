#include "sudokuboard.h"

#include <algorithm>
#include <ctime>

#include "resources.h"

#include <QDebug>

const unsigned NUMBERS_IN_ROW_COUNT = 9;
const int CELL_SIZE = 51;
const sf::Vector2f POSITION(340, 61);

SudokuBoard::SudokuBoard() :
    m_board(NUMBERS_IN_ROW_COUNT, std::vector<SudokuCell>(NUMBERS_IN_ROW_COUNT, SudokuCell(sf::Vector2f(CELL_SIZE, CELL_SIZE)))),
    m_currentCellBackground(sf::Vector2f(CELL_SIZE, CELL_SIZE))
{
    srand(time(nullptr));

    for (unsigned i = 0; i < m_board.size(); ++i)
    {
        for (unsigned j = 0; j < m_board[i].size(); ++j)
        {
            int iBorder = 2;
            int jBorder = 2;

            sf::Vector2f position(POSITION.x + (j * CELL_SIZE + (j * jBorder)),
                                  POSITION.y + (i * CELL_SIZE + (i * iBorder)));

            if (j >= 3)
                position.x += 2;

            if (j >= 6)
                position.x += 2;

            if (i >= 3)
                position.y += 2;

            if (i >= 6)
                position.y += 2;

            m_board[i][j].setPosition(position);
            m_board[i][j].setFont(Resources::getFont());
        }
    }

    m_currentCellBackground.setPosition(POSITION);
    m_currentCellBackground.setFillColor(sf::Color(10, 142, 241));

    setup();
}

bool SudokuBoard::generate()
{
    int row = 0;
    int col = 0;

    if (!findUnassignedLocation(row, col))
        return true;

    std::vector<int> validValues = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::random_shuffle(validValues.begin(), validValues.end());

    for (const auto& value : validValues)
    {
        if (canPlaceValue(row, col, value))
        {
            m_board[row][col].value = value;

            if (generate())
                return true;

            m_board[row][col].value = 0;
        }
    }

    return false;
}

SudokuCell &SudokuBoard::currentCell()
{
    return m_board[m_currentCellPosition.y][m_currentCellPosition.x];
}

void SudokuBoard::moveCurrentCell(const Movement &move)
{
    const int borderSize = 2;

    switch (move)
    {
    case Movement::Up:
        m_currentCellPosition.y--;
        m_currentCellBackground.move(0, -(CELL_SIZE + borderSize));

        if (m_currentCellPosition.y == 2 || m_currentCellPosition.y == 5)
            m_currentCellBackground.move(0, -borderSize);
        break;

    case Movement::Right:
        m_currentCellPosition.x++;
        m_currentCellBackground.move(CELL_SIZE + borderSize, 0);

        if (m_currentCellPosition.x == 3 || m_currentCellPosition.x == 6)
            m_currentCellBackground.move(borderSize, 0);
        break;

    case Movement::Down:
        m_currentCellPosition.y++;
        m_currentCellBackground.move(0, CELL_SIZE + borderSize);

        if (m_currentCellPosition.y == 3 || m_currentCellPosition.y == 6)
            m_currentCellBackground.move(0, borderSize);
        break;

    case Movement::Left:
        m_currentCellPosition.x--;
        m_currentCellBackground.move(-(CELL_SIZE + borderSize), 0);

        if (m_currentCellPosition.x == 2 || m_currentCellPosition.x == 5)
            m_currentCellBackground.move(-borderSize, 0);
        break;

    default:
        break;
    }
}

bool SudokuBoard::findUnassignedLocation(int &row, int &col) const
{
    for (unsigned i = 0; i < NUMBERS_IN_ROW_COUNT; ++i)
    {
        for (unsigned j = 0; j < NUMBERS_IN_ROW_COUNT; ++j)
        {
            if (m_board[i][j].value == 0)
                return true;

            col++;
        }

        row++;
        col = 0;
    }

    return false;
}

bool SudokuBoard::canPlaceValue(const int &row, const int &col, const int &value) const
{
    for (unsigned i = 0; i < NUMBERS_IN_ROW_COUNT; ++i)
    {
        if (m_board[row][i].value == value)
            return false;

        if (m_board[i][col].value == value)
            return false;
    }

    int iFloor = (row / 3) * 3;
    int jFloor = (col / 3) * 3;

    for (int i = iFloor; i < iFloor + 3; ++i)
    {
        for (int j = jFloor; j < jFloor + 3; ++j)
        {
            if (m_board[i][j].value == value)
                return false;
        }
    }

    return true;
}

void SudokuBoard::setup()
{
    if (!generate())
    {
        qDebug() << "Board can't be generated! :(";
    }

    for (int i = 0; i < 9; ++i)
    {
        for (int j = 0; j < 9; ++j)
        {
            qDebug() << "(" << i << "," << j << ")" << m_board[i][j].value;
        }
    }

    const int filledFieldsCount = rand() % 20 + 20;

    int counter = 0;
    m_filledCells = filledFieldsCount;

    sf::Vector2f pos;
    do
    {
        pos.x = rand() % NUMBERS_IN_ROW_COUNT;
        pos.y = rand() % NUMBERS_IN_ROW_COUNT;

        if (!m_board[pos.x][pos.y].isDefault())
        {
            int value = m_board[pos.x][pos.y].value;

            m_board[pos.x][pos.y].setDefault();
            m_board[pos.x][pos.y].setValue(value);
            counter++;
        }

    }
    while (counter < filledFieldsCount);

}

sf::Vector2u SudokuBoard::getCurrentCellPosition() const
{
    return m_currentCellPosition;
}

int SudokuBoard::numbersInRowCount() const
{
    return NUMBERS_IN_ROW_COUNT;
}

bool SudokuBoard::isCurrentCellSelected() const
{
    return m_board[m_currentCellPosition.x][m_currentCellPosition.y].isSelected();
}

void SudokuBoard::setCurrentCellValue(const int &value)
{
    m_board[m_currentCellPosition.x][m_currentCellPosition.y].setValue(value);
}

void SudokuBoard::increaseFilledCellsCount()
{
    m_filledCells++;
    qDebug() << m_filledCells;
}

void SudokuBoard::decreaseFilledCellsCount()
{
    m_filledCells--;
}

int SudokuBoard::filledCellsCount() const
{
    return m_filledCells;
}

bool SudokuBoard::isResolved() const
{
    for (const auto& cellsRow : m_board)
    {
        for (const auto& cell : cellsRow)
        {
            if (cell.userValue() != cell.value)
                return false;
        }
    }

    return true;
}

SudokuCell &SudokuBoard::getCell(const int &x, const int &y)
{
    return m_board[y][x];
}

void SudokuBoard::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(m_currentCellBackground, states);

    for (const auto& cellsRow : m_board)
    {
        for (const auto& cell : cellsRow)
        {
            target.draw(cell, states);
        }
    }
}
