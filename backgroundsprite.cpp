#include "backgroundsprite.h"

BackgroundSprite::BackgroundSprite()
{
    m_background.setTexture(Resources::getTexture(TextureType::Background));
    m_logo.setTexture(Resources::getTexture(TextureType::Logo));
    m_board.setTexture(Resources::getTexture(TextureType::Board));
    m_time.setTexture(Resources::getTexture( TextureType::Time));
}


void BackgroundSprite::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(m_background, states);
    target.draw(m_logo, states);
    target.draw(m_board, states);
    target.draw(m_time, states);
}
