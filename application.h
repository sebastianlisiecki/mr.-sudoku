#ifndef APPLICATION_H
#define APPLICATION_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "backgroundsprite.h"
#include "sudokuboard.h"

class Application
{
public:
    Application();

    void run();

private:
    void initialize();
    void handleEvents();
    void update();
    void draw();

    void saveGame();
    void loadGame();

    sf::RenderWindow m_window;
    sf::Event m_events;
    sf::Clock m_clock;
    sf::Text m_timeText;
    sf::Text m_statementText;
    sf::Sprite m_saveButton;
    sf::Sprite m_loadButton;

    BackgroundSprite m_background;
    SudokuBoard m_board;

    int m_time = 0;
    float m_timeElapsedFromLastUpdate = 0.0f;

    bool m_end = false;
};

#endif // APPLICATION_H
