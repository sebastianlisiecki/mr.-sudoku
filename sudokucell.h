#ifndef SUDOKUCELL_H
#define SUDOKUCELL_H

#include <SFML/Graphics.hpp>

#include <vector>

class SudokuCell : public sf::Drawable
{
public:
    SudokuCell(const sf::Vector2f& size);

    int value = 0;

    void setValue(const int& value);
    void setDefault();
    void setSelected();
    void clear();
    void reset();

    void setFont(const sf::Font& font);
    void setPosition(const sf::Vector2f& position);
    void setPosition(const float& x, const float& y);

    sf::Vector2f position() const;

    bool isDefault() const;
    bool isSelected() const;

    int userValue() const;

protected:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

private:
    sf::Text m_textValue;

    sf::Vector2f m_size;
    sf::Vector2f m_position;

    bool m_default = false;
    bool m_selected = false;
};

#endif // SUDOKUCELL_H
