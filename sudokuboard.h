#ifndef SUDOKUBOARD_H
#define SUDOKUBOARD_H

#include <SFML/Graphics.hpp>

#include <vector>

#include "sudokucell.h"

typedef std::vector<std::vector<SudokuCell>> Board;

enum class Movement
{
    Left,
    Right,
    Up,
    Down
};

class SudokuBoard : public sf::Drawable
{
public:
    SudokuBoard();

    bool generate();

    SudokuCell& currentCell();

    void moveCurrentCell(const Movement& move);

    sf::Vector2u getCurrentCellPosition() const;
    int numbersInRowCount() const;

    bool isCurrentCellSelected() const;

    void setCurrentCellValue(const int& value);

    void increaseFilledCellsCount();
    void decreaseFilledCellsCount();

    int filledCellsCount() const;

    bool isResolved() const;

    SudokuCell& getCell(const int& x, const int& y);

protected:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

private:
    bool findUnassignedLocation(int &row, int& col) const;
    bool canPlaceValue(const int& row, const int& col, const int& value) const;
    void setup();

    Board m_board;

    sf::RectangleShape m_currentCellBackground;
    sf::Vector2u m_currentCellPosition;

    int m_filledCells = 0;

};

#endif // SUDOKUBOARD_H
